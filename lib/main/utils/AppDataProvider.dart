import 'package:nb_utils/nb_utils.dart';
import 'package:prokit_flutter/fullApps/shopHop/screens/ShSplashScreen.dart';
import 'package:prokit_flutter/main/model/AppModel.dart';
import 'package:prokit_flutter/main/utils/AppConstant.dart';

List<LanguageDataModel> languageList() {
  return [
    LanguageDataModel(
        id: 1,
        name: 'English',
        languageCode: 'en',
        fullLanguageCode: 'en-US',
        flag: 'images/flag/ic_us.png'),
    LanguageDataModel(
        id: 2,
        name: 'Hindi',
        languageCode: 'hi',
        fullLanguageCode: 'hi-IN',
        flag: 'images/flag/ic_hi.png'),
    LanguageDataModel(
        id: 3,
        name: 'Arabic',
        languageCode: 'ar',
        fullLanguageCode: 'ar-AR',
        flag: 'images/flag/ic_ar.png'),
    LanguageDataModel(
        id: 4,
        name: 'French',
        languageCode: 'fr',
        fullLanguageCode: 'fr-FR',
        flag: 'images/flag/ic_fr.png'),
  ];
}

Future<AppTheme> getAllAppsAndThemes() async {
  AppTheme appTheme = AppTheme();

  appTheme.fullApp = getFullApps();
  appTheme.defaultTheme = getDefaultTheme();

  return appTheme;
}

// endregion

//region FullApps
ProTheme getFullApps() {
  ProTheme theme =
      ProTheme(name: "Full Apps", tag: 'New', show_cover: true, sub_kits: []);

  List<ProTheme> list = [];
  list.add(ProTheme(name: 'Shop hop', type: '', widget: ShSplashScreen()));

  theme.sub_kits!.addAll(list);
  return theme;
}

//endregion,isWebSupported: true

//endregion

//region Default Theme
ProTheme getDefaultTheme() {
  return ProTheme(
      name: "Default Theme",
      title_name: 'Default Theme',
      type: '',
      show_cover: false,
      darkThemeSupported: true,
      isWebSupported: true);
}
//endregion


//endregion
