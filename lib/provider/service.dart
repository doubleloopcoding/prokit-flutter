import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';

import 'package:prokit_flutter/fullApps/shopHop/models/ShProduct.dart';

String ip = "http://192.168.100.7/serviceflutter/";
String key = "123456";

showAlertDialog(BuildContext context, String title, String content) {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

checkresult(response, context, showDialog, f) async {
  Map result = new Map();

  try {
    if (response.statusCode == 400) {
      result["result"] = "no";
      result["code"] = response.statusCode;
      if (showDialog) {
        result["result"] = "no";
        result["code"] = response.statusCode;
        showAlertDialog(context, "Peringatan", "Time out");
      }
    } else if (response.statusCode == 200) {
      var rst = json.decode(response.body);
      if (rst["result"] == "no") {
        result["result"] = "no";
        result["code"] = response.statusCode;
        result["message"] = rst["message"];
        if (showDialog) {
          showAlertDialog(context, "Peringatan", rst["message"]);
        }
      } else {
        result["data"] = rst["data"];
        result["result"] = "ok";
      }
    } else {
      if (showDialog) {
        result["result"] = "no";
        result["peringatan"] = "Kesalahan service";
        showAlertDialog(context, "Peringatan", "Kesalahan service");
      }
      result["result"] = "no";
      result["code"] = response.statusCode;
    }
  } catch (error) {
    result["result"] = "no";
    result["code"] = 1001;
    if (showDialog) {
      showAlertDialog(context, "Peringatan", error.toString());
    }
  }

  f(result);
  return result;
}

Future checkserviceget(BuildContext context, Function f, String url,
    {bool showDialog = true}) async {
  if (showDialog) {
    showLoaderDialog(context);
  }

  final response = await http.get(Uri.parse(ip + url)).timeout(
    const Duration(seconds: 10),
    onTimeout: () {
      // Time has run out, do what you wanted to do.
      return http.Response(
          'Error', 408); // Request Timeout response status code
    },
  );

  if (showDialog) {
    Navigator.pop(context);
  }

  return checkresult(response, context, showDialog, f);
}

Future checkservicepost(
    BuildContext context, Function f, dynamic body, String url,
    {bool showDialog = true}) async {
  if (showDialog) {
    showLoaderDialog(context);
  }
  var param = jsonEncode(body);
  final response = await http
      .post(Uri.parse(ip + url),
          body: param,
          headers: {'Content-type': 'application/json', 'key': key},
          encoding: Encoding.getByName("utf-8"))
      .timeout(
    const Duration(seconds: 10),
    onTimeout: () {
      // Time has run out, do what you wanted to do.
      return http.Response(
          'Error', 408); // Request Timeout response status code
    },
  );
  ;

  if (showDialog) {
    Navigator.pop(context);
  }

  return checkresult(response, context, showDialog, f);
}

/*
Future checklistpost(BuildContext context, Function f, dynamic body, String url,
    {bool showDialog = true}) async {
  if (showDialog) {
    showLoaderDialog(context);
  }
  var param = jsonEncode(body);

  Map result = new Map();
  result["result"] = "ok";

  final response = await http
      .post(Uri.parse(ip + url),
          body: param,
          headers: {'Content-type': 'application/json', 'key': key},
          encoding: Encoding.getByName("utf-8"))
      .timeout(
    const Duration(seconds: 10),
    onTimeout: () {
      // Time has run out, do what you wanted to do.
      return http.Response(
          'Error', 408); // Request Timeout response status code
    },
  );
  ;

  if (showDialog) {
    Navigator.pop(context);
  }

  //Map result = new Map();

  return result;

  //f(result);
}
*/